
const axios = require('axios');
const settings = require('ep_pwxep/settings');
let token = null;



const connect = async function(){

    await axios.post('https://'+settings.pw_hostname+'/api/auth', {
        username: settings.pw_username,
        password: settings.pw_password
    }).then(res => {
        token = res.data.jwt;
        axios.defaults.withCredentials = true;
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token;
    }).catch(err => {
        console.log(err.response.status);
        console.log(err.message);
    });

}

exports.setPage = async function(pageID, padID){
    for(let retry = 0; retry < 2; retry++){
        if(token === null){
            await connect();
            console.log('got token: '+token);
        }
        
        await axios.post('https://'+settings.pw_hostname+'/api/posts', {
            pageID: pageID,
            padID:padID
        }).then(res => {
            console.log(res.data);
            return;
        }).catch(err => {
            console.log(err.response.status);
            console.log(err.response.data);
            console.log(err.message);
            token = null;
        });
        retry++;
    
    }


}