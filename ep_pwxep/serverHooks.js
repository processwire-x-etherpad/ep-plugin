const htmlExporter = require('ep_etherpad-lite/node/utils/ExportHtml');
//const markdownExporter = require('ep_markdown/exportMarkdown');
const padManager = require('ep_etherpad-lite/node/db/PadManager');
const restAPI = require('ep_pwxep/restAPI');


const pageIdFromPadId = function(padId){
    let matches = padId.match(/[0-9]+$/);
    console.log(matches); 
    return matches[0]; 
}


exports.padUpdate = async (hookName, {pad, author}) => {
    //console.log(pad);
}

// Using an async function:
exports.handleMessage = async (hookName, context) => {
    const message = context.message;
    const client = context.client;
   console.log('ok');
    if(message.type === 'COLLABROOM'){ 

        if(message.data !== undefined && message.data.type !== undefined && message.data.type === 'save_to_pw'){
            console.warn('pad id', message.data.padId);
            let padId = message.data.padId;
            //let pad = await htmlExporter.getPadHTMLDocument(padId);
            let pageId = pageIdFromPadId(padId);
            await restAPI.setPage(pageId, padId);
            
            /*
            let pad = await markdownExporter.getPadMarkdownDocument(padId, null, function(data, response){
                console.log(response);
                restAPI.getPage(1015);
            });
            //console.log(pad);
            
            //console.log(padId);
            */
        }
    }
    
  };