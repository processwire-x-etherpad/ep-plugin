# ep-plugin
ep_pwxep is an etherpad plugin that adds the functionality to save a pad content to a Processwire page when one saves a pad revision. It works in conjunction with the Processwire plugin PwToEp.

TODO: re-lock page when one modify the pad content after saving a revision (unlocking the page).